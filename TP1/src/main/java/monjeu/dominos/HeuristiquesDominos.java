package monjeu.dominos;

import jeux.alg.Heuristique;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;


public class HeuristiquesDominos{

	public static  Heuristique hblanc = new Heuristique(){
				
		public int eval(PlateauJeu p, Joueur j){
			PlateauDominos plateauDominos = (PlateauDominos) p;
			if(plateauDominos.finDePartie()){
				if(plateauDominos.isJoueurBlanc(j)){
					return 100000;
				} else {
					return -100000;
				}
			}
			return plateauDominos.nbCoupsBlanc() - plateauDominos.nbCoupsNoir();
		}
	};

	public static  Heuristique hnoir = new Heuristique(){
	
		public int eval(PlateauJeu p, Joueur j){
			PlateauDominos plateauDominos = (PlateauDominos) p;
			if(plateauDominos.finDePartie()){
				if(plateauDominos.isJoueurBlanc(j)){
					return -100000;
				} else {
					return 100000;
				}
			}
			return plateauDominos.nbCoupsNoir() - plateauDominos.nbCoupsBlanc();
		}
	};

}
