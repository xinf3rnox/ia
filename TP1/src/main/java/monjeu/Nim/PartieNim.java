package monjeu.Nim;

import jeux.alg.AlgoJeu;
import jeux.alg.Minimax;
import jeux.modele.CoupJeu;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;
import monjeu.dominos.HeuristiquesDominos;
import monjeu.dominos.PlateauDominos;

import java.util.ArrayList;

public class PartieNim {

    public static void main(String[] args) {

        Joueur jBlanc = new Joueur("Arthur");
        Joueur jNoir = new Joueur("Dieu");

        Joueur[] lesJoueurs = new Joueur[2];

        lesJoueurs[0] = jBlanc;
        lesJoueurs[1] = jNoir;


        AlgoJeu AlgoJoueur[] = new AlgoJeu[2];
        AlgoJoueur[0] = new Minimax(HeuristiquesNim.hblanc, jBlanc, jNoir,3); // Il faut remplir la méthode !!!
        AlgoJoueur[1] = new Minimax(HeuristiquesNim.hnoir, jNoir, jBlanc,4);  // Il faut remplir la méthode !!!

        System.out.println("Jeux n°1 : Algorithmes pour les Jeux");
        System.out.println("Etat Initial du plateau de jeu:");

        boolean jeufini = false;
        CoupJeu meilleurCoup = null;
        int jnum;

        PlateauJeu plateauCourant = new PlateauNim();
        PlateauNim.setJoueurs(jBlanc, jNoir);
        // Pour savoir qui joue "noir" et qui joue "blanc"


        // A chaque itération de la boucle, on fait jouer un des deux joueurs
        // tour a tour
        jnum = 0; // On commence par le joueur Blanc (arbitraire)

        while (!jeufini) {
            System.out.println("" + plateauCourant);
            System.out.println("C'est au joueur " + lesJoueurs[jnum] + " de jouer.");
            // Vérifie qu'il y a bien des coups possibles
            // Ce n'est pas tres efficace, mais c'est plus rapide... a écrire...
            ArrayList<CoupJeu> lesCoupsPossibles = plateauCourant.coupsPossibles(lesJoueurs[jnum]);
            System.out.println("Coups possibles pour" + lesJoueurs[jnum] + " : " + lesCoupsPossibles);

            // Lancement de l'algo de recherche du meilleur coup
            System.out.println("Recherche du meilleur coup avec l'algo " + AlgoJoueur[jnum]);
            meilleurCoup = AlgoJoueur[jnum].meilleurCoup(plateauCourant);
            if (meilleurCoup == null) {
                System.out.println("le joueur " + lesJoueurs[jnum] + " ne peut plus jouer");
                System.out.println("Le joueur " + lesJoueurs[1 - jnum] + " a gagné cette partie !");
                jeufini = true;
                break;
            }
            System.out.println("Coup joué : " + meilleurCoup + " par le joueur " + lesJoueurs[jnum]);

            plateauCourant.joue(lesJoueurs[jnum], meilleurCoup);
            // Le coup est effectivement joué

            if (plateauCourant.finDePartie()) {
                System.out.println("Le joueur " + lesJoueurs[jnum] + " viens de prendre la derniere allumette !");
                System.out.println("Le joueur " + lesJoueurs[1 - jnum] + " a gagné cette partie !");
                jeufini = true;
            }

            jnum = 1 - jnum;
        }
    }
}
