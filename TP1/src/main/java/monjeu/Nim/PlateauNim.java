package monjeu.Nim;

import java.util.ArrayList;

import jeux.modele.CoupJeu;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;

public class PlateauNim implements PlateauJeu {

    /* Pour coder un nouveau jeu... il faut au minimum coder
     * - Une classe PlateauX pour représenter l'état du "plateau"
     *  de ce jeu.
     *  Cette classe doit fournir le code des méthodes de l'interface PlateauJeu
     *  qui permettent de caractériser les règles du jeu
     *  Une classe CoupX qui
     */

    /** Le joueur que joue "Blanc" */
    private static Joueur joueurBlanc;

    /** Le joueur que joue "noir" */
    private static Joueur joueurNoir;


    /** Taille de la grille */
    public final static int TAILLE = 10;

    private int allumettes;

	public PlateauNim(){
        this.allumettes = TAILLE;
	}

	public PlateauNim(int taille){
        this.allumettes = taille;
    }


    /* A Faire */

    public ArrayList<CoupJeu> coupsPossibles(Joueur j) {
        ArrayList<CoupJeu> lesCoupsPossibles = new ArrayList<CoupJeu>();
        for(int i=1; i<= 3; i++){
            CoupJeu coupJeu = new CoupNim(i);
            if(coupValide(j,coupJeu)){
                lesCoupsPossibles.add(coupJeu);
            }
        }
        return lesCoupsPossibles;
    }

    public void joue(Joueur j, CoupJeu c) {
        this.allumettes -= ((CoupNim)c).enlever();
   }

    public boolean finDePartie() {
         return this.allumettes <= 0;
   }

    public PlateauJeu copy() {
        return new PlateauNim(this.allumettes);
  }

    public boolean coupValide(Joueur j, CoupJeu c) {
          return (this.allumettes - ((CoupNim)c).enlever()) >= 0 ;
    }

    @Override
    public String toString() {
        return "Il reste : "+ this.allumettes;
    }

    public int getAllumettes() {
        return allumettes;
    }

    /************* Gestion des paramètres de classe** ****************/

    public static void setJoueurs(Joueur jb, Joueur jn) {
        joueurBlanc = jb;
        joueurNoir = jn;
    }

    public boolean isJoueurBlanc(Joueur jb) {
        return joueurBlanc.equals(jb);
    }

    public boolean isJoueurNoir(Joueur jn) {
        return joueurNoir.equals(jn);
    }
}
