package monjeu.Nim;

import jeux.modele.CoupJeu;

public class CoupNim implements CoupJeu{

	private int c;

	public CoupNim(int c) {
		this.c = c;
	}

	public int enlever(){
		return this.c;
	}

	@Override
	public String toString() {
		return String.valueOf(c);
	}
}
