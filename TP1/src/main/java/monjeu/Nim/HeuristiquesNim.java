package monjeu.Nim;

import jeux.alg.Heuristique;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;
import monjeu.dominos.PlateauDominos;


public class HeuristiquesNim {

	public static  Heuristique hblanc = new Heuristique(){
				
		public int eval(PlateauJeu p, Joueur j){
			PlateauNim plateauNim = (PlateauNim) p;

			if(plateauNim.isJoueurBlanc(j)){
				if(plateauNim.getAllumettes() == 1){
					return Integer.MIN_VALUE;
				}
				if(plateauNim.getAllumettes() == 0){
					return Integer.MAX_VALUE;
				}
			} else {
				if(plateauNim.getAllumettes() == 1){
					return Integer.MAX_VALUE;
				}
				if(plateauNim.getAllumettes() == 0){
					return Integer.MIN_VALUE;
				}
			}
			return plateauNim.getAllumettes();
		}
	};

	public static  Heuristique hnoir = new Heuristique(){
	
		public int eval(PlateauJeu p, Joueur j){
			PlateauNim plateauNim = (PlateauNim) p;

			if(plateauNim.isJoueurNoir(j)){
				if(plateauNim.getAllumettes() == 1){
					return Integer.MIN_VALUE;
				}
				if(plateauNim.getAllumettes() == 0){
					return Integer.MAX_VALUE;
				}
			} else {
				if(plateauNim.getAllumettes() == 1){
					return Integer.MAX_VALUE;
				}
				if(plateauNim.getAllumettes() == 0){
					return Integer.MIN_VALUE;
				}
			}
			return plateauNim.getAllumettes();
		}
	};

}
