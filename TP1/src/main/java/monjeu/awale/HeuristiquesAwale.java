package monjeu.awale;

import jeux.alg.Heuristique;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;


public class HeuristiquesAwale{

	public static  Heuristique hblanc = new Heuristique(){
				
		public int eval(PlateauJeu p, Joueur j){
			PlateauAwale plateauAwale = (PlateauAwale) p;
			if(p.finDePartie()){
				if(plateauAwale.isJoueurBlanc(j) && j == plateauAwale.getWinner()){
					return Integer.MAX_VALUE;
				} else {
					return Integer.MIN_VALUE;
				}
			}
			return plateauAwale.getScoreblanc() - plateauAwale.getScorenoir();
		}
	};

	public static  Heuristique hnoir = new Heuristique(){
	
		public int eval(PlateauJeu p, Joueur j){
			PlateauAwale plateauAwale = (PlateauAwale) p;
			if(p.finDePartie()){
				if(plateauAwale.isJoueurNoir(j) && j == plateauAwale.getWinner()){
					return Integer.MAX_VALUE;
				} else {
					return Integer.MIN_VALUE;
				}
			}
			return plateauAwale.getScorenoir() - plateauAwale.getScoreblanc();
		}
	};

}
