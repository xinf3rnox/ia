package monjeu.awale;

import jeux.modele.CoupJeu;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;

import java.io.PrintStream;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class PlateauAwale implements PlateauJeu {

    /* *********** constantes *********** */

    /**
     * Taille de la grille
     */
    public final static int NB_JOUEUR = 2;
    public final static int TAILLE = 12;

    /* *********** Paramètres de classe *********** */

    private final static int PLEIN = 4;
    private final static int VIDE = 0;
    private final static int BLANC = 1;
    private final static int NOIR = 2;

    /**
     * Le joueur que joue "Blanc"
     */
    private static Joueur joueurBlanc;
    private int scoreblanc;
    /**
     * Le joueur que joue "noir"
     */
    private static Joueur joueurNoir;
    private int scorenoir;

    /* *********** Attributs  *********** */

    /**
     * le damier
     */
    private int damier[];

    /************* Constructeurs ****************/

    public PlateauAwale() {
        scoreblanc=0;
        scorenoir=0;
        LinkedList<Integer> blanc = new LinkedList<>();
        damier = new int[TAILLE];
        for (int j = 0; j < TAILLE; j++)
            damier[j] = PLEIN;
    }

    public PlateauAwale(int depuis[]) {
        scoreblanc=0;
        scorenoir=0;
        damier = new int[TAILLE];
        for (int j = 0; j < TAILLE; j++)
            damier[j] = depuis[j];
    }

    /************* Gestion des paramètres de classe** ****************/

    public static void setJoueurs(Joueur jb, Joueur jn) {
        joueurBlanc = jb;
        joueurNoir = jn;
    }

    public boolean isJoueurBlanc(Joueur jb) {
        return joueurBlanc.equals(jb);
    }

    public boolean isJoueurNoir(Joueur jn) {
        return joueurNoir.equals(jn);
    }


    /************* Méthodes de l'interface PlateauJeu ****************/

    public PlateauJeu copy() {
        return new PlateauAwale(this.damier);
    }

    public boolean coupValide(Joueur joueur, CoupJeu c) {
        CoupAwale cd = (CoupAwale) c;
        int index = cd.getIndex();
        return coupValide(joueur, index);
    }

    public ArrayList<CoupJeu> coupsPossibles(Joueur joueur) {
        ArrayList<CoupJeu> lesCoupsPossibles = new ArrayList<CoupJeu>();
        if (joueur.equals(joueurBlanc)) {
            for (int j = 0; j < TAILLE/2; j++) { // regarde sur une colonne
                if (coupValide(joueur,j)) // on peut jouer
                    lesCoupsPossibles.add(new CoupAwale(j));
            }
        } else { // Noir
            for (int j = TAILLE/2; j < TAILLE; j++) { // regarde sur toute colonne
                if (coupValide(joueur,j))  // on peut jouer
                    lesCoupsPossibles.add(new CoupAwale(j));
            }
        }
        return lesCoupsPossibles;
    }

    public boolean finDePartie() {
        return (scoreblanc >= 26 || scorenoir >= 26);
    }


    public void joue(Joueur joueur, CoupJeu c) {
        CoupAwale cd = (CoupAwale) c;
        int index = ((CoupAwale) c).getIndex();
        int pion = damier[index];
        damier[index] = 0;
        while (pion > 0) {
            index+=1;
            damier[index%12] += 1;
            pion -= 1;
        }

        if (index%12 >= TAILLE/2 && isJoueurBlanc(joueur)) {
            for (int x = index%12; x >= TAILLE/2; x--) {
                if (damier[x] == 2 || damier[x] == 3) {
                    //TODO: scoring
                    scoreblanc += damier[x];
                    damier[x] = 0;
                } else {
                    break;
                }
            }
        }
        if (index%12 < TAILLE/2 && isJoueurNoir(joueur)) {
            for (int x = index%12; x < TAILLE/2; x++) {
                if (damier[x] == 2 || damier[x] == 3) {
                    //TODO: scoring
                    scorenoir += damier[x];
                    damier[x] = 0;
                } else {
                    break;
                }
            }
        }
    }

    /* ********************* Autres méthodes ***************** */

    private boolean coupValide(Joueur joueur, int index) {
        if (joueur.equals(joueurBlanc)) {
            if (estaffamer(joueurNoir)) {
                //TODO: joueur un coup qui nourisse le joueur adversaire
                return damier[index]+index >= TAILLE/2;
            } else {
                //pas affamer
                return damier[index] != VIDE;
            }
        } else {
            if (estaffamer(joueurBlanc)) {
                //TODO: joueur un coup qui nourisse le joueur adversaire
                return damier[index]+index >= TAILLE;
            } else {
                //pas affamer
                return damier[index] != VIDE;
            }
        }
    }

    private boolean estaffamer(Joueur joueur) {
        if (isJoueurBlanc(joueur)) {
            return Arrays.stream(Arrays.copyOfRange(damier,0,TAILLE/2)).sum() == 0;
        } else {
            return Arrays.stream(Arrays.copyOfRange(damier,TAILLE/2,TAILLE)).sum() == 0;
        }
    }


    public String toString() {
        String retstr = new String("");
        retstr += "{"+scorenoir+"} ";
        for (int i = TAILLE-1; i >= TAILLE/2; i--) {
            retstr += damier[i] + "|";
        }
        retstr += "\n" + "------------------" + "\n";
        retstr += "{"+scoreblanc+"} ";
        for (int i = 0; i < TAILLE/2; i++) {
            retstr += damier[i] + "|";
        }
        return retstr;
    }

    public void printPlateau(PrintStream out) {
        out.println(this.toString());
    }

    public int getScoreblanc() {
        return scoreblanc;
    }

    public int getScorenoir() {
        return scorenoir;
    }

    public Joueur getWinner(){
        if(scoreblanc >= 26){
            return joueurBlanc;
        }
        return joueurNoir;
    }
}
