package monjeu.awale;
import jeux.modele.CoupJeu;

public class CoupAwale implements CoupJeu{

	/****** Attributs *******/ 

	private int index;

	/****** Clonstructeur *******/ 

	public CoupAwale(int l) {
		index = l;
	}

	/****** Accesseurs *******/ 

	public int getIndex() {
		return index;
	}

	/****** Accesseurs *******/

	public String toString() {
		return "("+index+")";
	}
}

