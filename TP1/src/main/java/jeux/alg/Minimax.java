/**
 * 
 */

package jeux.alg;

import java.util.ArrayList;

import jeux.modele.CoupJeu;
import jeux.modele.PlateauJeu;
import jeux.modele.joueur.Joueur;
import monjeu.dominos.PlateauDominos;

public class Minimax implements AlgoJeu {

    /** La profondeur de recherche par défaut
     */
    private final static int PROFMAXDEFAUT = 4;

   
    // -------------------------------------------
    // Attributs
    // -------------------------------------------
 
    /**  La profondeur de recherche utilisée pour l'algorithme
     */
    private int profMax = PROFMAXDEFAUT;

     /**  L'heuristique utilisée par l'algorithme
      */
   private Heuristique h;

    /** Le joueur Min
     *  (l'adversaire) */
    private Joueur joueurMin;

    /** Le joueur Max
     * (celui dont l'algorithme de recherche adopte le point de vue) */
    private Joueur joueurMax;

    /**  Le nombre de noeuds développé par l'algorithme
     * (intéressant pour se faire une idée du nombre de noeuds développés) */
       private int nbnoeuds;

    /** Le nombre de feuilles évaluées par l'algorithme
     */
    private int nbfeuilles;


  // -------------------------------------------
  // Constructeurs
  // -------------------------------------------
    public Minimax(Heuristique h, Joueur joueurMax, Joueur joueurMin) {
        this(h,joueurMax,joueurMin,PROFMAXDEFAUT);
    }

    public Minimax(Heuristique h, Joueur joueurMax, Joueur joueurMin, int profMaxi) {
        this.h = h;
        this.joueurMin = joueurMin;
        this.joueurMax = joueurMax;
        profMax = profMaxi;
//		System.out.println("Initialisation d'un MiniMax de profondeur " + profMax);
    }

   // -------------------------------------------
  // Méthodes de l'interface AlgoJeu
  // -------------------------------------------
   public CoupJeu meilleurCoup(PlateauJeu p) {
        /* A vous de compléter le corps de ce fichier */
       CoupJeu meilleurCoup =  p.coupsPossibles(this.joueurMax).get(0);
       int meilleur = Integer.MIN_VALUE;
       for(CoupJeu c : p.coupsPossibles(this.joueurMax)){
           PlateauJeu p2 = p.copy();
           p2.joue(this.joueurMax,c);
           int newCout = minMax(p2,this.profMax-1);
           if(newCout > meilleur){
               meilleurCoup = c;
               meilleur = newCout;
           }
       }
        return meilleurCoup;
    }
  // -------------------------------------------
  // Méthodes publiques
  // -------------------------------------------
    public String toString() {
        return "MiniMax(ProfMax="+profMax+")";
    }



  // -------------------------------------------
  // Méthodes internes
  // -------------------------------------------

    //A vous de jouer pour implanter Minimax
    private int maxMin(PlateauJeu p, int depth){
        ArrayList<CoupJeu> coupJeus = p.coupsPossibles(this.joueurMax);
        if(p.finDePartie() || depth <= 0){
            return this.h.eval(p,this.joueurMax);
        }
        int meilleur = Integer.MIN_VALUE;
        for(CoupJeu c : p.coupsPossibles(this.joueurMax)){
            PlateauJeu p2 = p.copy();
            if(p2.coupValide(this.joueurMax,c)){
                p2.joue(this.joueurMax,c);
                meilleur = Math.max(minMax(p2,depth-1),meilleur);
            }
        }
        return meilleur;
    }

    private int minMax(PlateauJeu p, int depth){
        ArrayList<CoupJeu> coupJeus = p.coupsPossibles(this.joueurMin);
        if(p.finDePartie() || depth <= 0){
            return this.h.eval(p,this.joueurMin);
        }
        int pire = Integer.MAX_VALUE;
        for(CoupJeu c : p.coupsPossibles(this.joueurMin)){
            PlateauJeu p2 = p.copy();
            if(p2.coupValide(this.joueurMin,c)){
                p2.joue(this.joueurMin,c);
                pire = Math.min(maxMin(p2,depth-1),pire);
            }
        }
        return pire;
    }
}
